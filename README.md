# Asteroids
<img src="https://gitlab.com/yuesubi-games/asteroids/-/raw/main/res/img/preview.png" width="640">

### English
This project was made for my computer science class, the objective was using
classes in a project.

### Français
Ce projet à été fait dans le cadre d'un projet de NSI, le but était d'utiliser
des classes.


## Launch the game / Lancer le jeu

### English
Make sure **pygame v2** is installed.

To move, either use the arrows or ZQSD. And to shot bullets, press space or the
left mouse button.

#### Windows
```cmd
python.exe src\\main.py
```
or
```cmd
C:\\path\\to\\python\\installation\\python.exe src\\main.py
```

#### Linux
```bash
python src/main.py
```
or
```bash
/path/to/python/installation/python src/main.py
```

### Français

Vérifier que **pygame v2** est installé.

Pour bouger utiliser les touches ZQSD ou les flèches et pour lancer des
projectilles appuyez sur espace ou le bouton de la souris.

#### Windows
```cmd
python.exe src\\main.py
```
ou
```cmd
C:\\dossier\\d\\installation\\de\\python\\python.exe src\\main.py
```

#### Linux
```bash
python src/main.py
```
ou
```bash
/dossier/d/installation/de/python/python src/main.py
```

## Progression / Progress

### English
* 1h00 Main loop and pygame setup 
* 0h25 Made the game manager
* 0h40 Added the time manager
* 1h00 Added the event manager
* 0h45 Player movements
* 0h50 Polygon
* 0h35 Bullets
* 1h10 Asteroids that are kept within the world space 
* 0h10 Bullets out of the screen are deleted
* 0h20 Bullet/asteroids collisions
* 0h45 Asteroids/player collisions
* 0h30 Score
* 0h20 Increasingly more asteroids
* 1h00 Code cleaning
* 1h00 Lifes
* 0h55 Game ends when out of lifes
* 0h40 Asteroids don't spawn on the player anymore
* 0h10 Buttons for the UI
* 0h35 Scenes & Menu scene
* 0h45 Score saver & changement in the scene management
* 1h00 Added sounds to the game

### Français
* 1h00 Boucle principale et initialisation de pygame 
* 0h25 Création du gestionnaire de jeu
* 0h40 Ajout du gestionnaire de temps
* 1h00 Ajout du gestionnaire d'évènements
* 0h45 Mouvements du joueur
* 0h50 Polygone
* 0h35 Ajout des projectiles
* 1h10 Ajout d'astéroides et Astéroïdes d’un coté à l’autre de l’écran
* 0h10 Les projectiles en dehors de l'écran sont supprimés
* 0h20 Collisions astéroïdes/projectiles
* 0h45 Collisions astéroïdes/joueur
* 0h30 Ajouter un score
* 0h20 De plus en plus d’astéroïdes
* 1h00 Nétoyage du code
* 1h00 Ajout de vies
* 0h55 Arrêt du jeu quand il n'y a plus de vies
* 0h40 Astéroïdes n'apparaissent pas sur le joueur
* 0h10 Ajout de boutons pour le UI
* 0h35 Ajout de scènes & création de la scène de menu
* 0h45 Sauvegarde du score et changement de la gestion des scènes
* 1h00 Ajout de sons au jeu


## Roadmap / Objectifs

### English
- [x] Entry point
- [x] Pygame setup
- [x] Game manager
- [x] ++ Time manager
- [x] ++ Event manager
- [x] The player
- [x] Player movement
- [x] ++ Polygon
- [x] Bullets
- [x] Asteroids
- [x] Asteroids kept in game bounds
- [x] Bullet/asteroids collisions
- [x] Asteroids/player collisions
- [x] Score
- [x] Increasingly more asteroids
- [x] ++ Asteroids don't spawn on the player
- [x] ++ Menu
- [x] ++ Scenes
- [x] ++ Save best and last score
- [x] ++ Sons

### Français
- [x] Fonction main
- [x] Mettre en place pygame
- [x] Gestionnaire de jeu
- [x] ++ Gestionnaire de temps
- [x] ++ Gestionnaire d'évènements
- [x] Ajout du joueur
- [x] Mouvement joueur
- [x] ++ Polygone
- [x] Ajout de projectiles
- [x] Ajout des astéroïdes
- [x] Astéroïdes d’un coté à l’autre de l’écran
- [x] Collisions astéroïdes/projectiles
- [x] Collisions astéroïdes/joueur
- [x] Ajouter un score
- [x] De plus en plus d’astéroïdes
- [x] ++ Astéroïdes n'apparaissent pas sur le joueur
- [x] ++ Menu
- [x] ++ Scènes
- [x] ++ Sauvegarde du meilleur et dernier score
- [x] ++ Sons


## Credits / Crédits

### English
This project uses some free assets from [Open Game Art](https://opengameart.org)
* [Another space background track](https://opengameart.org/content/another-space-background-track)
  by yd
* [SpaceShip shoting](https://opengameart.org/content/spaceship-shoting) by kindland

### Français
Ce projets utilise des musiques libres de droits d'[Open Game Art](https://opengameart.org)
* [Another space background track](https://opengameart.org/content/another-space-background-track)
  de yd
* [SpaceShip shoting](https://opengameart.org/content/spaceship-shoting) de kindland
