import pygame

from scene_manager import Scene, SceneManager
from event_manager import EventManager

from ui.text_button import UiTextButton


class DeathScreenScene(Scene):
    def __init__(self):
        """Constructeur de la scène de défaite."""

        self.play_again_button: UiTextButton = UiTextButton(
            pygame.Vector2(360, 210), pygame.Vector2(250, 50),
            "REJOUER", 28, (50, 50, 50),
            self.play_again_button_callback
        )
        self.back_to_menu_button: UiTextButton = UiTextButton(
            pygame.Vector2(360, 290), pygame.Vector2(250, 50),
            "ALLER AU MENU", 28, (50, 50, 50),
            self.back_to_menu_button_callback
        )
        
    def play_again_button_callback(self) -> None:
        """
        Fonction à appeller quand le bouton "jouer encore" est pressé. Change
        la scène pour aller au jeu.
        """
        SceneManager.switch_scene(1)
        
    def back_to_menu_button_callback(self) -> None:
        """
        Fonction à appeller quand le bouton "aller au menu" est pressé. Change
        la scène pour aller au menu principal.
        """
        SceneManager.switch_scene(0)
    
    def register_events(self, event_manager: EventManager) -> None:
        """
        Prendre en compte les événements (doit être impémentée dans une sous
        classe).
        :param event_manager: Le gestionnaire d'événements qui contient les
            entrés actuelles.
        """
        self.play_again_button.react_to_click(event_manager)
        self.back_to_menu_button.react_to_click(event_manager)

    def update_by(self, delta_time: float) -> None:
        """
        Actualiser de la scène (doit être impémentée dans une sous classe).
        :param delta_time: Le temps écoulé depuis la dernière actualisation de
            la scène.
        """
    
    def update_movements_by(self, move_delta_time: float) -> None:
        """
        Actualiser les mouvements de la scène (doit être impémentée dans une
        sous classe).
        :param move_delta_time: La quantitée de temps par laquelle bouger les
            éléments de la scène.
        """

    def render_to(self, target_surface: pygame.Surface) -> None:
        """
        Faire le rendu de la scène (doit être impémentée dans une sous classe).
        :param target_surface: La surface sur laquelle dessiner la scène.
        """
        self.play_again_button.render_to(target_surface)
        self.back_to_menu_button.render_to(target_surface)
