import os


PROJECT_PATH: str = os.path.join(os.path.dirname(__file__), "..")


class Assets:
    ASSETS_FOLDER: str = os.path.join(PROJECT_PATH, "res")
    
    AUDIO_FOLDER: str = os.path.join(ASSETS_FOLDER, "audio")
    DATA_FOLDER: str = os.path.join(ASSETS_FOLDER, "data")
    IMAGE_FOLDER: str = os.path.join(ASSETS_FOLDER, "img")

    PLAYER_SHOOT_AUDIO: str = os.path.join(AUDIO_FOLDER, "NovaShot.wav")
    ASTEROID_DESTROY_AUDIO: str = os.path.join(AUDIO_FOLDER, "asteroid_destroy.wav")
    BACKGROUND_AUDIO: str = os.path.join(AUDIO_FOLDER, "background.wav")
    
    SCORE_FILE: str = os.path.join(DATA_FOLDER, "score.json")
    
    GAME_ICON_IMAGE: str = os.path.join(IMAGE_FOLDER, "icon.png")
