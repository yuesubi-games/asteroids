from ui.ui_constants import *


class UiText:
    def __init__(self, position: pygame.Vector2, text_content: str,
                 font_size: int) -> None:
        """
        Constructeur de UiText.
        :param position: Position de l'élément
        :param text_content: Texte à afficher.
        :param font_size: Taille de la police.
        """
        self.position: pygame.Vector2 = position
        self.dimentions: pygame.Vector2 = pygame.Vector2(0, 0)

        self.font: pygame.font.Font = pygame.font.Font(FONT_STYLE, font_size)
        self.surface: pygame.Surface = pygame.Surface((0, 0))
        
        self.set_text_to(text_content)

    def set_text_to(self, text_content: str) -> None:
        """
        Changer le texte à afficher.
        :param text_content: Le nouveau texte.
        """
        self.surface = self.font.render(text_content, False, FONT_COLOR)

        self.dimentions.x = self.surface.get_width()
        self.dimentions.y = self.surface.get_height()

    def render_to(self, target_surface: pygame.Surface) -> None:
        """
        Dessiner le texte.
        :param target_surface: La surface sur laquelle dessiner.
        """
        top_left_position = self.position - self.dimentions / 2.0
        target_surface.blit(self.surface, top_left_position)
