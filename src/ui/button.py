from typing import Union

from ui.ui_constants import *

from event_manager import EventManager


class UiButton:
    def __init__(self, position: pygame.Vector2, dimentions: pygame.Vector2,
                 background_color: tuple[int, int, int],
                 callback: Union[callable, None] = None) -> None:
        """
        Constructeur de UiButton.
        :param position: Position de l'élément
        :param dimentions: Dimentions de l'élément
        :param callback: La fonction appelée lors d'un appui sur le bouton.
        """
        self.position: pygame.Vector2 = position
        self.dimentions: pygame.Vector2 = dimentions
        self.background_color: tuple[int, int, int] = background_color
        self.callback: Union[callable, None] = callback
    
    def react_to_click(self, event_manager: EventManager) -> None:
        """
        Appelée la fonction du bouton si un click est fait.
        :param event_manager: Le gestionnaire d'évenements.
        """
        mouse_position = pygame.Vector2(pygame.mouse.get_pos())
        
        clicked = (event_manager.is_button_pressed(1)
            and mouse_position.x > self.position.x - self.dimentions.x / 2.0
            and mouse_position.y > self.position.y - self.dimentions.y / 2.0
            and mouse_position.x < self.position.x + self.dimentions.x / 2.0
            and mouse_position.y < self.position.y + self.dimentions.y / 2.0
        )
        
        if clicked and self.callback is not None:
            self.callback()

    def render_to(self, target_surface: pygame.Surface) -> None:
        """
        Dessiner le bouton.
        :param target_surface: La surface sur laquelle dessiner.
        """
        rect = pygame.Rect(
            self.position.x - self.dimentions.x / 2.0,
            self.position.y - self.dimentions.y / 2.0,
            self.dimentions.x,
            self.dimentions.y
        )
        pygame.draw.rect(target_surface, self.background_color, rect)
