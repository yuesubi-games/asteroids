from typing import Union

from ui.ui_constants import *

from ui.button import UiButton
from ui.text import UiText
from event_manager import EventManager


class UiTextButton:
    def __init__(self, position: pygame.Vector2, dimentions: pygame.Vector2,
                 text_content: str, font_size: int,
                 background_color: tuple[int, int, int],
                 callback: Union[callable, None] = None) -> None:
        """
        Constructeur de UiTextButton.
        :param position: Position de l'élément
        :param dimentions: Dimentions de l'élément
        :param text_content: Texte à afficher.
        :param font_size: Taille de la police.
        :param callback: La fonction appelée lors d'un appui sur le bouton.
        """
        self.button: UiButton = UiButton(
            position, dimentions,
            background_color, callback
        )
        self.text: UiText = UiText(position, text_content, font_size)
    
    def react_to_click(self, event_manager: EventManager) -> None:
        """
        Appelée la fonction du bouton si un click est fait.
        :param event_manager: Le gestionnaire d'évenements.
        """
        self.button.react_to_click(event_manager)

    def set_text_to(self, text_content: str) -> None:
        """
        Changer le texte à afficher.
        :param text_content: Le nouveau texte.
        """
        self.text.set_text_to(text_content)

    def render_to(self, target_surface: pygame.Surface) -> None:
        """
        Dessiner le texte.
        :param target_surface: La surface sur laquelle dessiner.
        """
        self.button.render_to(target_surface)
        self.text.render_to(target_surface)
