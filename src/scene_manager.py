import pygame
from typing import Union

from event_manager import EventManager


class Scene:
    def register_events(self, event_manager: EventManager) -> None:
        """
        Prendre en compte les événements (doit être impémentée dans une sous
        classe).
        :param event_manager: Le gestionnaire d'événements qui contient les
            entrés actuelles.
        """

    def update_by(self, delta_time: float) -> None:
        """
        Actualiser de la scène (doit être impémentée dans une sous classe).
        :param delta_time: Le temps écoulé depuis la dernière actualisation de
            la scène.
        """
    
    def update_movements_by(self, move_delta_time: float) -> None:
        """
        Actualiser les mouvements de la scène (doit être impémentée dans une
        sous classe).
        :param move_delta_time: La quantitée de temps par laquelle bouger les
            éléments de la scène.
        """

    def render_to(self, target_surface: pygame.Surface) -> None:
        """
        Faire le rendu de la scène (doit être impémentée dans une sous classe).
        :param target_surface: La surface sur laquelle dessiner la scène.
        """


class SceneManager:
    current_scene: Union[Scene, None] = None
    create_scene_callback: callable = lambda _: None
    
    @classmethod
    def set_create_scene_callback(cls, create_callback: callable):
        """
        Changer la fonction utilisée pour créer une scène.
        :param create_callback: La fonction utilisée pour créer une nouvelle
            scène. Le fonction doit être de la forme
            def cb(scene_id: int) -> Scene:
        """
        cls.create_scene_callback = create_callback
    
    @classmethod
    def switch_scene(cls, new_scene_id: int):
        """
        Passer à une nouvelle scène.
        :param new_scene: L'instance d'une scène ou du classe héritant de scène
            à utiliser en tant que nouvelle scène.
        """
        cls.current_scene = cls.create_scene_callback(new_scene_id)
    
    # Fonctions qui appellent les méthodes de la scène
    
    @classmethod
    def register_events(cls, event_manager: EventManager) -> None:
        """
        Appelle current_scene.register_events si current_scene n'est pas None,
        voir Scene.register_events pour plus de détails.
        """
        if cls.current_scene is not None:
            cls.current_scene.register_events(event_manager)

    @classmethod
    def update_by(cls, delta_time: float) -> None:
        """
        Appelle current_scene.update_by si current_scene n'est pas None, voir
        Scene.update_by pour plus de détails.
        """
        if cls.current_scene is not None:
            cls.current_scene.update_by(delta_time)

    @classmethod
    def update_movements_by(cls, move_delta_time: float) -> None:
        """
        Appelle current_scene.update_movements_by si current_scene n'est pas
        None, voir Scene.update_movements_by pour plus de détails.
        """
        if cls.current_scene is not None:
            cls.current_scene.update_movements_by(move_delta_time)

    @classmethod
    def render_to(cls, target_surface: pygame.Surface) -> None:
        """
        Appelle current_scene.render_to si current_scene n'est pas None, voir
        Scene.render_to pour plus de détails.
        """
        if cls.current_scene is not None:
            cls.current_scene.render_to(target_surface)
