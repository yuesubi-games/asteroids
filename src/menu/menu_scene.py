import pygame

from scene_manager import Scene, SceneManager
from event_manager import EventManager
from score_manager import ScoreManager

from ui.text import UiText
from ui.text_button import UiTextButton


class MenuScene(Scene):
    def __init__(self):
        """Constructeur de la scène de menu."""
        
        last_score = ScoreManager.get_last_score()
        self.last_score_text: UiText = UiText(
            pygame.Vector2(200, 220),
            str(last_score), 64
        )
        self.last_score_label_text: UiText = UiText(
            pygame.Vector2(200, 160),
            "DERNIER", 28
        )

        best_score = ScoreManager.get_best_score()
        self.best_score_text: UiText = UiText(
            pygame.Vector2(440, 220),
            str(best_score), 64
        )
        self.best_score_label_text: UiText = UiText(
            pygame.Vector2(440, 160),
            "MEILLEUR", 28
        )

        self.play_button: UiTextButton = UiTextButton(
            pygame.Vector2(320, 300), pygame.Vector2(150, 50),
            "JOUER", 28, (50, 50, 50),
            self.play_button_callback
        )
        
    def play_button_callback(self) -> None:
        SceneManager.switch_scene(1)
    
    def register_events(self, event_manager: EventManager) -> None:
        """
        Prendre en compte les événements (doit être impémentée dans une sous
        classe).
        :param event_manager: Le gestionnaire d'événements qui contient les
            entrés actuelles.
        """
        self.play_button.react_to_click(event_manager)

    def update_by(self, delta_time: float) -> None:
        """
        Actualiser de la scène (doit être impémentée dans une sous classe).
        :param delta_time: Le temps écoulé depuis la dernière actualisation de
            la scène.
        """
    
    def update_movements_by(self, move_delta_time: float) -> None:
        """
        Actualiser les mouvements de la scène (doit être impémentée dans une
        sous classe).
        :param move_delta_time: La quantitée de temps par laquelle bouger les
            éléments de la scène.
        """

    def render_to(self, target_surface: pygame.Surface) -> None:
        """
        Faire le rendu de la scène (doit être impémentée dans une sous classe).
        :param target_surface: La surface sur laquelle dessiner la scène.
        """
        target_surface.fill((0, 0, 0))
        
        self.last_score_text.render_to(target_surface)
        self.best_score_text.render_to(target_surface)
        
        self.last_score_label_text.render_to(target_surface)
        self.best_score_label_text.render_to(target_surface)
        
        self.play_button.render_to(target_surface)