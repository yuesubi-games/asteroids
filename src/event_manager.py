import pygame


class EventManager:
    def __init__(self) -> None:
        """Constructeur."""

        self._keys_pressed: dict[int, bool] = {}
        self._keys_released: dict[int, bool]   = {}
        self._keys_down: dict[int, bool]      = {}

        self._buttons_pressed: dict[int, bool] = {}
        self._buttons_released: dict[int, bool]   = {}
        self._buttons_down: dict[int, bool]      = {}

        self.quit: bool = False

    def is_key_pressed(self, key: int) -> bool:
        """
        Savoir si une touche est pressée.
        :param key: L'identifiant de la touche.
        :return: Vrai si la touche est pressée.
        """
        return self._keys_pressed.get(key, False)
 
    def is_key_released(self, key: int) -> bool:
        """
        Savoir si une touche est relachée.
        :param key: L'identifiant de la touche.
        :return: Vrai si la touche est relachée.
        """
        return self._keys_released.get(key, False)
    
    def is_key_down(self, key: int) -> bool:
        """
        Savoir si une touche est enfoncée.
        :param key: L'identifiant de la touche.
        :return: Vrai si la touche est enfoncée.
        """
        return self._keys_down.get(key, False)
    
    def is_button_pressed(self, button: int) -> bool:
        """
        Savoir si un bouton est pressée.
        :param key: L'identifiant du bouton.
        :return: Vrai si le bouton est pressée.
        """
        return self._buttons_pressed.get(button, False)

    def is_button_released(self, button: int) -> bool:
        """
        Savoir si un bouton est relachée.
        :param key: L'identifiant du bouton.
        :return: Vrai si le bouton est relachée.
        """
        return self._buttons_released.get(button, False)
    
    def is_button_down(self, button: int) -> bool:
        """
        Savoir si un bouton est enfoncée.
        :param key: L'identifiant du bouton.
        :return: Vrai si le bouton est enfoncée.
        """
        return self._buttons_down.get(button, False)

    def update_events(self) -> None:
        """
        Actualiser les évenements avec les nouvelles entrées arrivées.
        """

        # Retirer les évenements précédents
        self._keys_pressed: dict[int, bool] = {}
        self._keys_released: dict[int, bool]   = {}

        self._buttons_pressed: dict[int, bool] = {}
        self._buttons_released: dict[int, bool]   = {}

        self.quit: bool = False

        # Actualise les évenements
        for event in pygame.event.get():
            # Détection de touches
            if event.type == pygame.KEYDOWN:
                self._keys_pressed[event.key] = True
                self._keys_down[event.key] = True
            elif event.type == pygame.KEYUP:
                self._keys_released[event.key] = True
                self._keys_down[event.key] = False

            # Détection de boutons
            elif event.type == pygame.MOUSEBUTTONDOWN:
                self._buttons_pressed[event.button] = True
                self._buttons_down[event.button] = True
            elif event.type == pygame.MOUSEBUTTONUP:
                self._buttons_released[event.button] = True
                self._buttons_down[event.button] = False

            # Demande de fermeture de la fenêtre
            elif event.type == pygame.QUIT:
                self.quit = True
