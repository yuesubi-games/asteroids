import pygame
import os


GAME_NAME: str = "Asteroids"

ICON_NAME:str = "icon.png"
RESOURCE_FOLDER: str = "res"
ICON_PATH: str = os.path.join(os.path.dirname(__file__), "..", "..", RESOURCE_FOLDER, ICON_NAME)

GAME_SIZE: pygame.Vector2 = pygame.Vector2((640, 480))
GAME_EXTRA_BORDER: float = 48.0

MOVEMENT_UPDATE_FPS: float = 100.0


# Paramèttres pour l'affichage des vies
UI_LIFES_POSITION: pygame.Vector2 = pygame.Vector2(10, 32)
UI_LIFES_SPACING: float = 4.0
UI_LIFES_SIZE: float = 16.0
UI_LIFES_COLOR: pygame.Color = pygame.Color(200, 200, 200)


# Paramettres pour la création d'astéroïdes
INITIAL_SPAWN_INTERVAL: float = 10.0
MIN_SPAWN_INTERVAL: float = 1.0
SPAWN_INTERVAL_CHANGE: float = 0.85

ASTEROID_MIN_SIZE: float = 30.0
ASTEROID_MAX_SIZE: float = 70.0

ASTEROID_MIN_SPEED: float = 50.0  # 10.0
ASTEROID_MAX_SPEED: float = 180.0  # 70.0
ASTEROID_MIN_TORQUE: float = 10.0
ASTEROID_MAX_TORQUE: float = 100.0  # 70.0

ASTEROID_MIN_SPAWN_DISTANCE_FROM_PLAYER: float = 150.0
