import imp


import random
import pygame

from game.polygon import Polygon


RIGHT_VECTOR = pygame.Vector2(1, 0)


class Asteroid:
    LINE_WIDTH: float = 2.0
    
    @staticmethod
    def generate_random_polygon(side_amount: int) -> Polygon:
        """
        Génere un polygone pour un asteroïd.
        :param side_amount: Le nombre de cotés du polygone.
        :return: Le polygone créé.
        """
        polygon = Polygon()

        # Ajout du point central
        polygon.verticies.append(pygame.Vector2(0, 0))
        
        # Créer les points du polygone
        angle_step = 360.0 / float(side_amount)
        for side in range(side_amount):
            # Création du point
            vertex = RIGHT_VECTOR.rotate(angle_step * side)
            vertex *= float(random.randint(20, 100)) / 100.0

            # Ajouter le point au polygone
            polygon.verticies.append(vertex)
            polygon.indices.append(side + 1)

            # Ajouter le triangle formé entre le centre, un poit et le suivant 
            polygon.triangles.append((
                0, side + 1,
                (side + 1) % side_amount + 1)
            )
        
        return polygon
    
    def __init__(self, position: pygame.Vector2, velocity: pygame.Vector2,
                 size: float, torque: float) -> None:
        """
        Construit un astéroïde.
        :param position: La position de départ de l'astéroïde.
        :param velocity: La vélocitée de l'astéroïde.
        :param size: La taille de l'astéroïde.
        :param torque: La vitesse de rotation de l'astéroïde.
        """
        
        self.position: pygame.Vector2 = position
        self.rotation: float = 0.0
        self.size: float = size

        self.velocity: pygame.Vector2 = velocity
        self.torque: float = torque
        
        # Création d'un polygone avec un nombre de cotés aléatoire
        side_amount = random.randint(3, 12)
        self.polygon: Polygon = Asteroid.generate_random_polygon(side_amount)
        
    def collides_with_point(self, point: pygame.Vector2) -> None:
        """
        Regarde si le point est dans l'astéroïde.
        :param point: Le point à vérifier.
        :return: Vrai si le point est dans l'astéroïde.
        """
        return self.polygon.collides_with_point(point)

    def collides_with_polygon(self, polygon: Polygon) -> None:
        """
        Regarde si le polygone touche l'astéroïde.
        :param polygon: Le polygone à vérifier.
        :return: Vrai si le polygone touche l'astéroïde.
        """
        return self.polygon.collide_with_polygon(polygon)
    
    def update_movement_by(self, move_delta_time: float) -> None:
        """
        Actualiser les mouvements de l'astéroïde.
        :param move_delta_time: La quantitée de temps par laquelle bouger
            l'astéroïde.
        """
        self.position += self.velocity * move_delta_time
        self.rotation += self.torque * move_delta_time
        
        self.update_polygon_position_fields()
    
    def render_to(self, target_surface: pygame.Surface) -> None:
        """
        Faire le rendu de l'astéroïde.
        :param target_surface: La surface sur laquelle dessiner l'astéroïde.
        """
        self.polygon.render_to(
            target_surface, (0, 255, 255), int(self.LINE_WIDTH)
        )

    def update_polygon_position_fields(self) -> None:
        """
        Actualise la position, la rotation et la taille du polygone de
        l'astéroïde.
        """
        self.polygon.position = self.position
        self.polygon.rotation = self.rotation
        self.polygon.size = self.size
