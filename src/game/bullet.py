import pygame


class Bullet:
    SPEED: float = 164.0
    SIZE: float = 8.0
    THICKNESS: float = 2.0
    COLOR: pygame.Color = pygame.Color(100, 20, 200)
    
    def __init__(self, position: pygame.Vector2, rotation: float,
                 velocity: pygame.Vector2) -> None:
        """
        Constructeur du projectile.
        :param position: La position de départ du projectile.
        :param rotation: L'orientation du projectile.
        :param velocity: La velocitée du projectille.
        """
        self.position: pygame.Vector2 = position
        self.rotation = rotation

        self.velocity: pygame.Vector2 = velocity

    def update_movement_by(self, move_delta_time: float) -> None:
        """
        Actualiser la position du projectile.
        :param move_delta_time: La quantitée de temps par laquelle bouger.
        """
        self.position += self.velocity * move_delta_time
    
    def render_to(self, target_surface: pygame.Surface) -> None:
        """
        Dessiner le projectile.
        :param target_surface: La surface sur laquelle dessiner le projectile.
        """
        direction_vector = pygame.Vector2(1, 0).rotate(self.rotation)
        start_position = self.position - direction_vector * (self.SIZE / 2.0)
        end_position = self.position + direction_vector * (self.SIZE / 2.0)

        pygame.draw.line(target_surface, self.COLOR, start_position,
                         end_position, int(self.THICKNESS))
