import itertools
import pygame
import random

from game.asteroid import Asteroid
from game.bullet import Bullet
from game.player import Player, PlayerInputs

from game.game_constants import *

from event_manager import EventManager
from scene_manager import Scene, SceneManager
from score_manager import ScoreManager
from sound_manager import SoundManager

from ui.text import UiText


RIGHT_VECTOR = pygame.Vector2(1, 0)


class GameScene(Scene):
    def __init__(self, game_border_size: pygame.Vector2,
                 extra_border_for_entities: float) -> None:
        """
        Construire un gestionnaire de jeu.
        :param game_border_size: La largeure et la hauteure de la
            zone de jeu.
        :param extra_border_for_entities: La taille de la bordure
            avant que les elements soit transportés de l'autre coté
            de l'écran.
        """

        # Limites du jeu
        self.game_size: pygame.Vector2 = game_border_size
        self.extra_border: float = extra_border_for_entities

        # Variables pour le score
        self.score: int = 0
        self.score_text: UiText = UiText(
            pygame.Vector2(50, 20),
            f"Score: 0", 20
        )

        # Création du joueur
        game_center_position = game_border_size / 2.0
        self.player: Player = Player(game_center_position.copy())
        self.player_inputs: PlayerInputs = PlayerInputs()
        
        # Listes de projectiles et d'astéroÏdes
        self.bullets: list[Bullet] = list()
        self.asteroids: list[Asteroid] = list()
        
        # Paramèttes d'apparition des astéroÏdes
        self.time_since_last_asteroid_spawn: float = 8.0
        self.asteroid_spawn_interval: float = INITIAL_SPAWN_INTERVAL

    def register_events(self, event_manager: EventManager) -> None:
        """
        Enregistrer les évenements.
        :param event_manager: Le gestionnaire d'événements qui contient les
            entrés actuelles.
        """
        # Faire avancer le joueur si demandé
        self.player_inputs.forward = (
            event_manager.is_key_down(pygame.K_UP) or
            event_manager.is_key_down(pygame.K_z)
        )
        
        # Tourner le joueur si demandé
        self.player_inputs.turn_left = (
            event_manager.is_key_down(pygame.K_LEFT) or
            event_manager.is_key_down(pygame.K_q)
        )
        self.player_inputs.turn_right = (
            event_manager.is_key_down(pygame.K_RIGHT) or
            event_manager.is_key_down(pygame.K_d)
        )

        # Tirer si demandé
        self.player_inputs.shoot = (
            event_manager.is_key_pressed(pygame.K_SPACE) or
            event_manager.is_key_pressed(pygame.K_KP_ENTER) or
            event_manager.is_button_pressed(1)
        )
        
    def update_by(self, delta_time: float) -> None:
        """
        Lancer l'actualisation du jeu si le jeu n'est pas fini.
        :param delta_time: Le temps écoulé depuis la dernière actualisation du
            jeu.
        """
        
        if self.player.lifes > 0:
            self.update_game_by(delta_time)
        else:
            self.end_game()
    
    def end_game(self) -> None:
        """Mettre fin au jeu et aller au menu de partie finie."""
        best_score = ScoreManager.get_best_score()
        if self.score > best_score:
            ScoreManager.set_best_score(self.score)

        ScoreManager.set_last_score(self.score)
        SceneManager.switch_scene(2)

    def update_game_by(self, delta_time: float) -> None:
        """
        Actualiser le jeu.
        :param delta_time: Le temps écoulé depuis la dernière
            actualisation du jeu.
        """

        # Faire avancer le joueur si nécessaire
        if self.player_inputs.forward:
            self.player.add_forward_force(Player.SPEED * delta_time)
        
        # Faire tourner le joueur
        turn_amount = 0.0
        if self.player_inputs.turn_left:
            turn_amount -= 1.0
        if self.player_inputs.turn_right:
            turn_amount += 1.0
            
        torque = turn_amount * Player.ROTATION_SPEED * delta_time
        self.player.rotate_by(torque)

        # Tirer un projectille si nécessaire
        if self.player_inputs.shoot:
            self.spawn_bullet()
            SoundManager.play_player_shoot()
            
        # Création d'asteroïdes
        self.time_since_last_asteroid_spawn += delta_time
        
        # Actualiser le joueur
        self.player.update_by(delta_time)

        # Faire apparaître des astéroides
        if self.time_since_last_asteroid_spawn > self.asteroid_spawn_interval:
            self.spawn_asteroid()
            self.time_since_last_asteroid_spawn = 0.0

            self.asteroid_spawn_interval *= SPAWN_INTERVAL_CHANGE
            if self.asteroid_spawn_interval < MIN_SPAWN_INTERVAL:
                self.asteroid_spawn_interval = MIN_SPAWN_INTERVAL
        
    def update_movements_by(self, move_delta_time: float) -> None:
        """
        Lancer l'actualisation des mouvements du jeu si le jeu n'est pas fini.
        :param move_delta_time: La quantitée de temps par laquelle bouger les
            objets du jeu.
        """
        if self.player.lifes > 0:
            self.update_game_movements_by(move_delta_time)

    def update_game_movements_by(self, move_delta_time: float) -> None:
        """
        Actualiser les mouvements du jeu.
        :param move_delta_time: La quantitée de temps par laquelle bouger les
            objets du jeu.
        """

        self.player.update_movement_by(move_delta_time)
        self.position_in_game_space_of(self.player.position)
        
        for bullet in self.bullets:
            bullet.update_movement_by(move_delta_time)
        
        for asteroid in self.asteroids:
            asteroid.update_movement_by(move_delta_time)
            self.position_in_game_space_of(asteroid.position)
        
        self.delete_out_of_game_bullets()
        self.resolve_collisions()

    def delete_out_of_game_bullets(self) -> None:
        """Supprimer les projectiles sortis de la zone de jeu."""
        
        # Trouver tout les projectiles sortis
        bullets_to_delete = list()
        for b, bullet in enumerate(self.bullets):
            if not self.is_bullet_in_game_space(bullet):
                bullets_to_delete.append(b)
        
        # Supprimer les derniers projectiles de la liste d'abord pour éviter de
        # décaler tout les élements après celui supprimé et donc de supprimer
        # les mauvais projectiles.
        for to_delete_bullet_index in reversed(bullets_to_delete):
            self.bullets.pop(to_delete_bullet_index)

    def resolve_collisions(self) -> None:
        """Gestion des collisions entres les différents éléments du jeu."""
        self.resolve_asteroid_asteroid_collisions()
        self.resolve_asteroid_bullet_collisions()
        self.resolve_asteroid_player_collisions()
    
    def resolve_asteroid_asteroid_collisions(self) -> None:
        """Gestion des collisions entres les astéroïdes."""
        # Eléments à supprimer
        asteroids_to_delete = set()
        
        # Trouver toutes les différentes paires d'index d'astéroides posibles
        asteroid_pairs = itertools.combinations(range(len(self.asteroids)), 2)

        for asteroid_0_index, asteroid_1_index in asteroid_pairs:
            asteroid_0 = self.asteroids[asteroid_0_index]
            asteroid_1 = self.asteroids[asteroid_1_index]

            if asteroid_0.collides_with_polygon(asteroid_1.polygon):
                # Pas besoin de vérifier si les éléments sont déjà dans le set
                # avant de les ajouter, parce qu'un set ne peut pas contenir
                # deux fois la même valeure.
                asteroids_to_delete.add(asteroid_0_index)
                asteroids_to_delete.add(asteroid_1_index)

        # Supprimer les derniers astéroïdes des listes d'abord pour éviter de
        # décaler tout les élements après ceux supprimés et donc de supprimer
        # les mauvais astéroïdes.
        for asteroid_index in sorted(asteroids_to_delete, reverse=True):
            self.asteroids.pop(asteroid_index)
            SoundManager.play_asteroid_destroy()
    
    def resolve_asteroid_bullet_collisions(self) -> None:
        """Gestion des collisions entres les astéroïdes et les projectiles."""
        # Eléments à supprimer
        bullets_to_delete = set()
        asteroids_to_delete = set()
        
        # Paires d'indexes de toute les combinaisons d'astéroïdes et de
        # projectiles possibles.
        indecies_pairs = itertools.product(
            range(len(self.bullets)),
            range(len(self.asteroids))
        )
        
        for bullet_index, asteroid_index in indecies_pairs:
            asteroid = self.asteroids[asteroid_index]
            bullet_position = self.bullets[bullet_index].position
            
            # Simplification, ici on considère que le projectile ne touche
            # l'asteroïde que lorsque sa position est dans l'asteroïde. Le
            # projectile étant très petit, cette aproximation ne cause pas de
            # différences à l'oeil de l'utilisateur.
            if asteroid.collides_with_point(bullet_position):
                # Pas besoin de vérifier si les éléments sont déjà dans le set
                # avant de les ajouter, parce qu'un set ne peut pas contenir
                # deux fois la même valeure.
                bullets_to_delete.add(bullet_index)
                asteroids_to_delete.add(asteroid_index)
                
                # Incrémentation du score
                self.score += 1
                self.score_text.set_text_to(f"Score: {self.score}")

        # Supprimer les derniers projectiles et astéroïdes des listes d'abord
        # pour éviter de décaler tout les élements après ceux supprimés et donc
        # de supprimer les mauvais projectiles et astéroïdes.
        for bullet_index in sorted(bullets_to_delete, reverse=True):
            self.bullets.pop(bullet_index)
        for asteroid_index in sorted(asteroids_to_delete, reverse=True):
            self.asteroids.pop(asteroid_index)
            SoundManager.play_asteroid_destroy()

    def resolve_asteroid_player_collisions(self) -> None:
        """Gestion des collisions entres les astéroïdes et le joueur."""

        for asteroid in self.asteroids:
            # Si l'astéroïde touche le joueur et que le joueur n'est pas immunisé
            if (asteroid.polygon.collide_with_polygon(self.player.polygon) and
                not self.player.is_immune):
    
                # Enlever une vie et rendre le joueur invincible
                self.player.lifes = max(0, self.player.lifes - 1)
                self.player.immunize()

    def render_to(self, target_surface: pygame.Surface) -> None:
        """
        Faire le rendu du jeu.
        :param target_surface: La surface sur laquelle dessiner le jeu.
        """

        # Fond d'écran noir
        target_surface.fill((0, 0, 0))

        for bullet in self.bullets:
            bullet.render_to(target_surface)
        
        for asteroid in self.asteroids:
            asteroid.render_to(target_surface)
            
        self.player.render_to(target_surface)
        
        self.score_text.render_to(target_surface)
        self.render_lifes(target_surface)
    
    def render_lifes(self, target_surface: pygame.Surface) -> None:
        """Dessiner les vies du joueur."""
        for i in range(self.player.lifes):
            life_offset = pygame.Vector2(
                UI_LIFES_SIZE * (i + 0.5) + UI_LIFES_SPACING * i,
                UI_LIFES_SIZE/2
            )
            life_center = UI_LIFES_POSITION + life_offset

            pygame.draw.circle(target_surface, UI_LIFES_COLOR, life_center,
                UI_LIFES_SIZE/2, 2)
    
    def spawn_bullet(self) -> None:
        """Créer un projectile dans le jeu"""
        shoot_velocity = RIGHT_VECTOR.rotate(self.player.rotation)
        shoot_velocity *= Bullet.SPEED

        self.bullets.append(Bullet(
            self.player.position.copy(),
            self.player.rotation,
            self.player.velocity + shoot_velocity 
        ))
    
    def spawn_asteroid(self) -> None:
        """Créer un astéroïde dans le jeu"""
        
        # Position aléatoire
        position = pygame.Vector2(
            random.randint(0, int(GAME_SIZE.x)),
            random.randint(0, int(GAME_SIZE.y))
        )
        
        # Trouver si la distance avec le joueur est trop petite
        min_distance = ASTEROID_MIN_SPAWN_DISTANCE_FROM_PLAYER ** 2
        if (position - self.player.position).length_squared() < min_distance:

            # Calculer de combien il faut décaler le point d'apparition pour
            # que l'astéroïde n'apparaisse pas sur le joueur.
            offset = (position - self.player.position).normalize()
            offset *= ASTEROID_MIN_SPAWN_DISTANCE_FROM_PLAYER

            # Décaler le point d'apparition de l'astéroïde
            position += offset
        
        # Vitesse aléatoire
        speed = random.randint(
            int(ASTEROID_MIN_SPEED),
            int(ASTEROID_MAX_SPEED)
        )
        # Vélocité aléatoire
        velocity = pygame.Vector2(speed, 0).rotate(random.randint(0, 360))
        
        # Création de l'astéroïde
        self.asteroids.append(Asteroid(
                position, velocity,
                random.randint(ASTEROID_MIN_SIZE, ASTEROID_MAX_SIZE),
                random.randint(ASTEROID_MIN_TORQUE, ASTEROID_MAX_TORQUE)
        ))
        
    def is_bullet_in_game_space(self, bullet: Bullet) -> bool:
        """
        Vérifier si le projectile est dans l'espace de jeu.
        :param bullet: Le projectile à vérifier.
        """
        # Les positions maximum et minimum valides
        minimum_position_accepted = pygame.Vector2(
            -self.extra_border,
            -self.extra_border
        )
        maximum_position_accepted = pygame.Vector2(
            self.game_size.x + self.extra_border,
            self.game_size.y + self.extra_border
        )
        
        in_game_space = (
            bullet.position.x > minimum_position_accepted.x and
            bullet.position.y > minimum_position_accepted.y and
            bullet.position.x < maximum_position_accepted.x and
            bullet.position.y < maximum_position_accepted.y
        )

        return in_game_space
    
    def position_in_game_space_of(self,position: pygame.Vector2
                                  ) -> pygame.Vector2:
        """
        Passer la position de l'autre coté de l'écran si elle est en dehors des
        limites.
        :param position: La position à rectifier.
        """
        # Les positions maximum et minimum valides
        minimum_position_accepted = pygame.Vector2(
            -self.extra_border,
            -self.extra_border
        )
        maximum_position_accepted = pygame.Vector2(
            self.game_size.x + self.extra_border,
            self.game_size.y + self.extra_border
        )
        
        # Dimention de la zone de jeu
        game_dimentions = pygame.Vector2(
            2.0 * self.extra_border + self.game_size.x,
            2.0 * self.extra_border + self.game_size.y
        )

        # Corriger la position
        if position.x < minimum_position_accepted.x:
            position.x += game_dimentions.x
        if position.y < minimum_position_accepted.y:
            position.y += game_dimentions.y
        if position.x > maximum_position_accepted.x:
            position.x -= game_dimentions.x
        if position.y > maximum_position_accepted.y:
            position.y -= game_dimentions.y
