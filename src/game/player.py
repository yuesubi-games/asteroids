import math
import pygame

from game.polygon import Polygon


RIGHT_VECTOR = pygame.Vector2(1, 0)


class PlayerInputs:
    def __init__(self) -> None:
        self.forward: bool = False
        self.turn_left: bool = False
        self.turn_right: bool = False
        self.shoot: bool = False


class Player:
    SIZE: float = 16.0
    SPEED: float = 100.0
    ROTATION_SPEED: float = 200.0
    FRICTION_FACTOR: float = 0.5
    
    INITIAL_LIFES: int = 3
    IMMUNE_TIME: float = 3.0
    
    # Les couleures du joueur
    COLOR: tuple[int, int, int] = (255, 0, 255)
    IMMUNE_BLEND_COLOR: tuple[int, int, int] = (255, 200, 0)

    def __init__(self, position: pygame.Vector2) -> None:
        """Construire un joueur."""

        # Position et velocitée du joueur
        self.position: pygame.Vector2 = position
        self.velocity: pygame.Vector2 = pygame.Vector2(0, 0)

        self.rotation: float = 0.0
        
        # Imunitée du joueur pour quand il pert une vie
        self.is_immune: bool = False
        self.immune_time_bank: float = 0.0
        
        # Les vies du joueur
        self.lifes = self.INITIAL_LIFES

        # Forme du joueur
        self.polygon = Polygon()
        self.polygon.verticies = [
            pygame.Vector2(-0.5, -0.5),  # 0  | Schema:  0 ___
            pygame.Vector2(-0.3,  0.0),  # 1  |           \   ---___
            pygame.Vector2(-0.5,  0.5),  # 2  |           1>     ___ => 3
            pygame.Vector2( 1.0,  0.0)   # 3  |           /___---
        ]                                #    |          2
        self.polygon.indices = [0, 1, 2, 3]
        self.polygon.triangles = [(0, 1, 2), (1, 2, 3)]

    def update_by(self, delta_time: float) -> None:
        """
        Actualiser le joueur.
        :param delta_time: Le temps écoulé depuis la dernière actualisation.
        """
        if self.is_immune:
            self.immune_time_bank += delta_time
            
            # Désactiver l'immunitée après un certain temps
            if self.immune_time_bank > self.IMMUNE_TIME:
                self.is_immune = False

    def update_movement_by(self, move_delta_time: float) -> None:
        """
        Actualiser les mouvements du joueur.
        :param move_delta_time: La quantitée de temps par laquelle bouger le
            joueur.
        """

        # Player friction
        friction_amount = self.FRICTION_FACTOR * move_delta_time
        if friction_amount > 1.0:
            friction_amount = 1.0
        self.velocity *= (1.0 - friction_amount)

        # Actualiser la position et la velocitée du joueur
        self.position += self.velocity * move_delta_time

    def render_to(self, target_surface: pygame.Surface) -> None:
        """
        Faire le rendu du joueur.
        :param target_surface: La surface sur laquelle dessiner le joueur.
        """

        # Dessiner le polygone du joueur
        self.update_polygon_position_fields()
        self.polygon.render_to(target_surface, self.find_color(), 2)
        
    def find_color(self) -> tuple[int, int, int]:
        """Trouver la couleure du joueur."""
        # La couleure normale du joueur
        color = self.COLOR
        
        # Si le joueur est invincible faire des effets de couleurs
        if self.is_immune:
            # Donne un nombre entre 1.0 et 0.0 qui change doucement entre
            # chaque affichage.
            alpha = math.sin(self.immune_time_bank * math.pi * 2.0) / 2.0 + 0.5
            
            # Coefficients des deux couleures à mélanger
            immune_color = 1.0 - alpha
            normal_color = alpha
            
            # Calculer la couleure
            color = (
                self.COLOR[0] * normal_color + self.IMMUNE_BLEND_COLOR[0] * immune_color,
                self.COLOR[1] * normal_color + self.IMMUNE_BLEND_COLOR[1] * immune_color,
                self.COLOR[2] * normal_color + self.IMMUNE_BLEND_COLOR[2] * immune_color,
            )
        
        return color

    def add_forward_force(self, forward_force: float) -> None:
        """
        Ajouter une force dans la direction du joueur.
        :param forward_force: L'intessité de la force à ajouter.
        """
        self.velocity += RIGHT_VECTOR.rotate(self.rotation) * forward_force
    
    def rotate_by(self, rotation_amount: float) -> None:
        """
        Pivoter le joueur.
        :param rotation_amount: La quantitée par laquelle pivoter.
        """
        self.rotation += rotation_amount
    
    def immunize(self) -> None:
        """Rendre le joueur invincible."""
        self.is_immune = True
        self.immune_time_bank = 0.0

    def update_polygon_position_fields(self) -> None:
        """
        Actualise la position, la rotation et la taille du polygone de
        l'astéroïde.
        """
        self.polygon.position = self.position
        self.polygon.rotation = self.rotation
        self.polygon.size = self.SIZE
