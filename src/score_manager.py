from typing import Any
import json
import os

from assets import Assets


BEST_SCORE_NAME: str = "best_score"
LAST_SCORE_NAME: str = "last_score"
EMPTY_SCORE_DICT: dict = { BEST_SCORE_NAME: 0, LAST_SCORE_NAME: 0 }


class ScoreManager:
    @staticmethod
    def get_best_score() -> int:
        """
        Récupérer le meilleur score enregistré.
        :return: Le meilleur score.
        """
        ScoreManager.create_file_if_non_existant()
        score_dict = ScoreManager.open_to_read()
        return score_dict[BEST_SCORE_NAME]
    
    @staticmethod
    def get_last_score() -> int:
        """
        Récupérer le dernier score enregistré.
        :return: Le dernier score.
        """
        ScoreManager.create_file_if_non_existant()
        score_dict = ScoreManager.open_to_read()
        return score_dict[LAST_SCORE_NAME]
    
    @staticmethod
    def set_best_score(new_best_score: int) -> None:
        """
        Changer le meilleur score enregistré.
        :param new_best_score: Le nouveau meilleur score.
        """
        ScoreManager.create_file_if_non_existant()
        score_dict = ScoreManager.open_to_read()
        score_dict[BEST_SCORE_NAME] = new_best_score
        ScoreManager.write_dict(score_dict)
    
    @staticmethod
    def set_last_score(new_last_score: int) -> None:
        """
        Changer le dernier score enregistré.
        :param new_last_score: Le dernier meilleur score.
        """
        ScoreManager.create_file_if_non_existant()
        score_dict = ScoreManager.open_to_read()
        score_dict[LAST_SCORE_NAME] = new_last_score
        ScoreManager.write_dict(score_dict)
    
    @staticmethod
    def create_file_if_non_existant() -> None:
        """Créer le fichier et le dossier pour le score si il n'existe pas."""
        
        # Création du dossier si inéxistant
        score_file_dir = os.path.dirname(Assets.SCORE_FILE)
        if not os.path.isdir(score_file_dir):
            os.mkdir(score_file_dir)
        
        # Création du fichier si inéxistant
        if not os.path.isfile(Assets.SCORE_FILE):
            ScoreManager.write_dict(EMPTY_SCORE_DICT)
    
    @staticmethod
    def open_to_read() -> Any:
        """
        Récupérer le dictionnaire du fichier de score.
        :return: Le dictionnaire du fichier json.
        """
        score_dict = None
        with open(Assets.SCORE_FILE, "r+", encoding="utf-8") as score_file:
            score_dict = json.load(score_file)
        return score_dict
    
    @staticmethod
    def write_dict(new_dict: Any) -> None:
        """
        Remplacer le fichier du dictionnaire du fichier de score.
        :param new_dict: Le nouveau dictionnaire.
        """
        with open(Assets.SCORE_FILE, "w+", encoding="utf-8") as score_file:
            json.dump(new_dict, score_file)
