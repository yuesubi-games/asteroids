import time


MAX_TIME_SINCE_LAST_UPDATE_MOVEMENT = 0.2


class TimeManager:
    def __init__(self, update_movement_fps: float) -> None:
        """
        Constructeur

        :param fixed_update_fps: Le nombre d'actualisation par seconde de la
            boucle de mouvement.
        """
        self.delta_time: float = 0.0
        self._last_frame_time: int = 0
    
        self.movement_delta_time: float = 1.0 / update_movement_fps
        self._time_since_last_update_movement: float = 0.0

    def update_times(self) -> None:
        """
        Actualise les variables de temps.
        """
        new_time = time.perf_counter_ns()
        self.delta_time = (new_time - self._last_frame_time) / 1.0e9
        self._last_frame_time = new_time

        self._time_since_last_update_movement += self.delta_time
        # Permet de réguler le nombre maximum d'actualisation à faire
        self._time_since_last_update_movement = min(
            self._time_since_last_update_movement,
            MAX_TIME_SINCE_LAST_UPDATE_MOVEMENT
        )
    
    def should_update_movement(self) -> bool:
        """
        Demander si les mouvements devraient être actualisés.
        :return: Vrai si les mouvements doivent être actualisés.
        """
        return self._time_since_last_update_movement > self.movement_delta_time
    
    def updated_movement(self) -> None:
        """
        Enregister que les mouvements on été actualisés
        """
        self._time_since_last_update_movement -= self.movement_delta_time
