from typing import Union
import pygame

from assets import Assets




class SoundManager:
    audio_available: bool = True
    
    PLAYER_SHOOT_SOUND: Union[pygame.mixer.Sound, None] = None
    ASTROID_DESTROY_SOUND: Union[pygame.mixer.Sound, None] = None
    
    try:
        # Initialsiser le module de son de pygame
        pygame.mixer.init()
        
        PLAYER_SHOOT_SOUND = pygame.mixer.Sound(Assets.PLAYER_SHOOT_AUDIO)
        ASTROID_DESTROY_SOUND = pygame.mixer.Sound(Assets.ASTEROID_DESTROY_AUDIO)
        pygame.mixer.music.load(Assets.BACKGROUND_AUDIO)
        
    except pygame.error:
        # Désactivation du son a cause d'une erreure
        audio_available = False
    
    @classmethod
    def play_player_shoot(cls) -> None:
        """Jouer le son du joueur qui lance un projectile."""
        if cls.audio_available:
            cls.PLAYER_SHOOT_SOUND.play()
    
    @classmethod
    def play_asteroid_destroy(cls) -> None:
        """Jouer le son de l'astéroïde qui se casse."""
        if cls.audio_available:
            cls.ASTROID_DESTROY_SOUND.play()
    
    @classmethod
    def play_background(cls) -> None:
        """Lancer la musique du jeu."""
        if cls.audio_available:
            pygame.mixer.music.play(-1)
