from typing import Union

import pygame

from time_manager import TimeManager
from event_manager import EventManager
from scene_manager import Scene, SceneManager
from score_manager import ScoreManager
from sound_manager import SoundManager

from menu.menu_scene import MenuScene
from death_screen.death_screen import DeathScreenScene

from game.game_scene import GameScene
from game.game_constants import *

from assets import Assets


def change_scene_callback(new_scene_id: int) -> Union[Scene, None]:
    """
    Retourne une instance de la scène chosie.
    :param new_scene_id: L'identifiant de la scène.
    """
    new_scene = None
    
    if new_scene_id == 0:
        new_scene = MenuScene()
    elif new_scene_id == 1:
        new_scene = GameScene(GAME_SIZE, GAME_EXTRA_BORDER)
    else:
        new_scene = DeathScreenScene()
    
    return new_scene

def main() -> None:
    """Programme principal"""

    # Initialisation de la librairie
    pygame.init()
    
    # Mettre le titre et les dimentions de la fenêtre
    pygame.display.set_caption(GAME_NAME)
    window = pygame.display.set_mode(GAME_SIZE)
    pygame.display.set_icon(pygame.image.load(Assets.GAME_ICON_IMAGE))

    # Gestionnaires de temps, d'évenements et du jeu
    time_manager = TimeManager(MOVEMENT_UPDATE_FPS)
    event_manager = EventManager()
    
    # Attribuer les scène à leurs identifiants et démarer la première scène
    SceneManager.set_create_scene_callback(change_scene_callback) 
    SceneManager.switch_scene(0)
    
    # Jouer la musique d'arrière plan
    SoundManager.play_background()
    
    # Boucle principale du jeu avec game_running pour controller si le jeu doit
    # continuer à tourner
    game_running = True
    while game_running:
        # Acualisation des évenements
        event_manager.update_events()
        SceneManager.register_events(event_manager)

        # Fermer la fenetre si demandé
        if event_manager.quit:
            game_running = False

        time_manager.update_times()
        SceneManager.update_by(time_manager.delta_time)

        # Actualisation des mouvement
        while time_manager.should_update_movement():
            SceneManager.update_movements_by(
                time_manager.movement_delta_time
            )
            time_manager.updated_movement()
        
        # Affichage de la scène a l'écran
        SceneManager.render_to(window)
        pygame.display.flip()
    
    # Quitter pygame
    pygame.quit()


# Commencer le programme si ce fichier est lancé
if __name__ == '__main__':
    main()
